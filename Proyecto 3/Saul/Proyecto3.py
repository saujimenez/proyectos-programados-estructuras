# USAGE
# python detect_color.py --image pokemon_games.png

# import the necessary packages
from shapedetector import ShapeDetector
import numpy as np
import argparse
import imutils
import cv2
import random

todosNodos = []
todasConexiones = []
grafo = {}

class Nodo:
    
    #Clase Nodo para instanciar las figuras con sus respectiva ID, tipo de figura y color
    
    def __init__(self, figura, color, contorno, cX, cY, ID = None):
        self.figura = figura
        self.color = color
        self.contorno = contorno
        self.cX = cX
        self.cY = cY
        self.ID = ID
    
    def setID(self, ID):
        self.ID = ID

class Conexion_Figura:
    
    def __init__(self, figura):
        self.cabeza = figura
        self.siguiente = None
        self.peso=random.randint(1,99)
        
    def agregar_conexiones(self, nuevaFigura):
        """
            Función encargada de comprobar si el nodo nuevaFigura cumple los requisitos para formar parte de los nodos anexos
            E: un objeto de la clase Nodo
            S: N/A
            R: N/A
        """
        temp = self
        if nuevaFigura.figura == temp.cabeza.figura:
            while temp.siguiente != None:
                temp = temp.siguiente
            temp.siguiente = Conexion_Figura(nuevaFigura)
            del temp
        
        elif nuevaFigura.color == temp.cabeza.color:
            while temp.siguiente != None:
                temp = temp.siguiente
            temp.siguiente = Conexion_Figura(nuevaFigura)
            del temp
        
        else:
            del temp

    def cantidad_nodos(self):
        """
            Función que determina la cantidad de nodos anexos que tiene un Nodo
            E: N/A
            S: entero con la cantidad de nodos anexos
            R: N/A
        """
        temp = self.siguiente
        cantidad = 0
        
        while temp != None:
            cantidad += 1
            temp = temp.siguiente
        
        return cantidad
    
    def getConexiones(self):
        """
            Función encargada retornar los nodos anexos
            E: N/A
            S: una lista con tuplas, cada tupla tiene la ID y el peso de un nodo anexo al que se está analizando
            R: N/A
        """
        
        conexiones = []
        
        temp = self.siguiente
        
        while temp != None:
            tup = (temp.cabeza.ID, temp.peso)
            conexiones += [tup]
            temp = temp.siguiente
        return conexiones
    
    def mostrar_conexiones(self, imagen):
        """
            Función encargada de comprobar mostrar al usuario con imágenes los nodos anexos a el nodo enviado
            E: la imagen original
            S: N/A
            R: N/A
        """
        temp = self
        cv2.drawContours(imagen, [temp.cabeza.contorno], -1, (0, 0, 0), 4)
        cv2.putText(imagen, str(temp.cabeza.ID), (temp.cabeza.cX, temp.cabeza.cY), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (255, 255, 255), 2)
        cv2.imshow("Figura", imagen)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
        temp = self.siguiente
        while temp != None:
            cv2.drawContours(imagen, [temp.cabeza.contorno], -1, (38, 0, 77), 2)
            cv2.putText(imagen, str(temp.cabeza.ID), (temp.cabeza.cX, temp.cabeza.cY), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 1)
            temp = temp.siguiente
        cv2.imshow("Figura y sus amigos", imagen)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    
    def toString(self):
        """
            Función encargada de imprimir en la terminal una representación de cada nodo y sus respectivos nodos anexos
            E: un objeto de la clase Nodo
            S: N/A
            R: N/A
        """
        temp = self
        print("\n------------------------------------------------------------------------")
        print("ID: ", temp.cabeza.ID)
        print("Figura: ", temp.cabeza.figura)
        print("Color: ", temp.cabeza.color)
        print("Esta figura se junta con ", self.cantidad_nodos(), "nodos")
        temp = temp.siguiente
        while temp != None:
            print("  Un", temp.cabeza.figura.lower(), "de color", temp.cabeza.color.lower(), "(ID:", str(temp.cabeza.ID) + "), peso hacia esta figura:", temp.peso)
            temp = temp.siguiente
        print("------------------------------------------------------------------------")
 



def prim(origen):
    
    listaVisitados = []
    grafoResultante = {}
    listaOrdenada = []

    #    COMIENZO DEL ALGORITMO DE PRIM
    #1.- PEDIRLE AL USUARIO EL NODO ORIGEN
    #2.- AGREGARLO A LA LISTA DE VISITADOS
    listaVisitados.append(origen)

    #3.- AGREGAR SUS ADYACENTES A LA LISTA ORDENADA
    for destino, peso in grafo[origen]:
        listaOrdenada.append((origen, destino, peso))
    '''ORDENAMIENTO INSERT PARA LA LISTA'''
    pos=0
    act=0
    listAux=[]
    for i in range(len(listaOrdenada)):
        listAux=listaOrdenada[i]
        act=listaOrdenada[i][2]
        pos=i
        while pos> 0 and listaOrdenada[pos-1][2] > act:
            listaOrdenada[pos] = listaOrdenada[pos-1]
            pos=pos-1
        listaOrdenada[pos]=listAux


    
    #4.- MIENTRAS LA LISTA ORDENADA NO ESTE VACIA, HACER:
    while listaOrdenada:
        #5.-TOMAR VERTICE DE LA LISTA ORDENADA Y ELIMINARLO
        vertice = listaOrdenada.pop(0)
        d = vertice[1]
        
        #6.-SI EL DESTINO NO ESTA EN LA LISTA DE VISITADOS
        if d not in listaVisitados:
            #7.- AGREGAR A LA LISTA DE VISITADOS EN NODO DESTINO
            listaVisitados.append(d)
            #8.- AGREGAR A LA LISTA ORDENADA LOS ADYACENTES DEL NODO DESTINO 
            #"d" QUE NO HAN SIDO VISITADOS
            for key, lista in grafo[d]:
                if key not in listaVisitados:
                    listaOrdenada.append((d, key, lista))
            #####ORDENAMIENTO APLICADO A LA LISTA :
            listaOrdenada = [(c,a,b) for a,b,c in listaOrdenada]
            listaOrdenada.sort()
            listaOrdenada = [(a,b,c) for c,a,b in listaOrdenada]
            #9.-AGREGAR VERTICE AL GRAFO RESULTANTE
            # PARA COMPRENDER MEJOR, EN LAS SIGUIENTES LINEAS SE TOMA EL "VERTICE", QUE EN ESTE CASO
            # ES UNA TUPLA QUE CONTIENE TRES VALORES; EL VERTICE EN SU POSICIÓN 0 ES EL VALOR DEL NODO ORIGEN
            # EL VÉRTICE EN SU POSICIÓN 1 ES EL NODO DESTINO, Y EL VÉRTICE EN SU POSICIÓN 2 ES EL PESO DE LA ARISTA ENTRE AMBOS NODOS,
            # Y A CONTINUACIÓN SE AGREGAN ESOS VALORES AL GRAFO
            origen  = vertice[0]
            destino = vertice[1]
            peso    = vertice[2]

            if origen in grafoResultante:
                if destino in grafoResultante:
                    lista = grafoResultante[origen]
                    grafoResultante[origen] = lista + [(destino, peso)]
                    lista = grafoResultante[destino]
                    lista.append((origen, peso))
                    grafoResultante[destino] = lista
                else:
                    grafoResultante[destino] = [(origen, peso)]
                    lista = grafoResultante[origen]
                    lista.append((destino, peso))
                    grafoResultante[origen] = lista
            elif destino in grafoResultante:
                grafoResultante[origen] = [(destino, peso)]
                lista = grafoResultante [destino]
                lista.append((origen, peso))
                grafoResultante[destino] = lista
            else:
                grafoResultante[destino] = [(origen, peso)]
                grafoResultante[origen] = [(destino, peso)]
          
    print("\n\nGrafo resultante:\n")
    for key, lista in grafoResultante.items():
        print(key, end=" ")
        #print(lista)
    print("\n")



def detectar_figuras(imagen, color):
    """
        Función encargada de detectar las figuras en una imagen
        Esta función crea dos listas, una con los nodos y otra con los nodos y sus conexiones
        E: una imagen para analizar, el color de las figuras que se están analizando
        S: N/A
        R: N/A
    """
    
    #cv2.imshow("prueba color " + color, imagen)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    resized = imutils.resize(imagen, width=300)
    ratio = imagen.shape[0] / float(resized.shape[0])
    
    gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
    _, threshold = cv2.threshold(gray, 15, 255, cv2.THRESH_BINARY)
    #cv2.imshow("prueba negra color " + color, threshold)
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    cnts = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    cnts = imutils.grab_contours(cnts)
    sd = ShapeDetector()
    figuras = 0
    nodos = []
    global todasConexiones
    global todosNodos
        
        # loop over the contours
    for c in cnts:
        # compute the center of the contour, then detect the name of the
        # shape using only the contour
        M = cv2.moments(c)
        if M["m00"] < 40:
            continue
        #print("debí entrar aqui")
        figuras += 1
        cX = int((M["m10"] / M["m00"]) * ratio)
        cY = int((M["m01"] / M["m00"]) * ratio)
        shape = sd.detect(c)
        
        # multiply the contour (x, y)-coordinates by the resize ratio,
        # then draw the contours and the name of the shape on the image
        c = c.astype("float")
        c *= ratio
        c = c.astype("int")
        cv2.drawContours(imagen, [c], -1, (0, 255, 0), 2)
        cv2.putText(imagen, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        
        nodo_actual = Nodo(shape, color, c, cX, cY)
        nodos.append(nodo_actual)
        conActual = Conexion_Figura(nodo_actual)
        todasConexiones.append(conActual)
        # show the output image
        #cv2.imshow(color, imagen)
        #cv2.waitKey(0)
        #cv2.destroyAllWindows()
    
    todosNodos += nodos



def main():
    """
        Función que controla el flujo del programa
        E: N/A
        S: N/A
        R: N/A
    """
    eleccion = 1

    while eleccion != 0:
        print("\nMenú Principal")
        print("1. Mostrar imagen")
        print("2. Mostrar conexiones de un nodo especifico")
        print("3. Mostrar conexiones de todos los nodos")
        print("3. Mostrar camino mínimo de un nodo a otro")
        print("4. Obtener árbol recubridor mínimo")
        print("0. Salir")
        eleccion = input("Ingrese su elección: ")
        print()
        
        #Validacion
        if len(eleccion) != 1:
            continue
        #Validacion
        if ord(eleccion) < 48 or ord(eleccion) > 57:
            continue
        
        eleccion = int(eleccion)
        
        if eleccion == 1:
            cv2.imshow("Grafo con sus nodos", imagenNodos)
            cv2.waitKey(0)
            cv2.destroyAllWindows()
            
        elif eleccion == 2:
            ID = input("Ingrese el ID de la figura. *Ingrese 0 para salir\nID: ")

            
            #Validacion de que ingrese solo números
            
            if len(ID) != 1 and len(ID) != 2:
                print("\nIngresó un número no valido\n")
                continue
            if len(ID) == 1:
                if ord(ID) < 48 or ord(ID) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                else:
                    ID = int(ID)-1
            elif len(ID) == 2:
                if ord(ID[0]) < 48 or ord(ID[0]) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                elif ord(ID[1]) < 48 or ord(ID[1]) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                else:
                    ID = int(ID)-1
            
            
            if 0 < ID > len(todasConexiones):
                print("\nNo existe una figura asociada con la ID ingresada\n")
            elif ID != -1:
                todasConexiones[ID].toString()
                todasConexiones[ID].mostrar_conexiones(image.copy())
            else:
                continue
        
        elif eleccion == 3:
            for nodos in todasConexiones:
                nodos.toString()
                nodos.mostrar_conexiones(image.copy())
        
        elif eleccion == 4:
            ID = input("Ingrese la ID del Nodo por donde desea iniciar. *Ingrese 0 para salir\nID: ")
        
            #Validacion de que ingrese solo números
            
            if len(ID) != 1 and len(ID) != 2:
                print("\nIngresó un número no valido\n")
                continue
            if len(ID) == 1:
                if ord(ID) < 48 or ord(ID) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                else:
                    ID = int(ID)
            elif len(ID) == 2:
                if ord(ID[0]) < 48 or ord(ID[0]) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                elif ord(ID[1]) < 48 or ord(ID[1]) > 57:
                    print("\nIngresó un número no valido\n")
                    continue
                else:
                    ID = int(ID)
            
            if 0 < ID > len(todasConexiones):
                print("\nNo existe una figura asociada con la ID ingresada\n")
            elif ID != 0:
                prim(ID)
            else:
                continue
    return 0
    
    


# construct the argument parse and parse the arguments
#ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--image", help = "path to the image")
#·args = vars(ap.parse_args())
# load the image
image = cv2.imread("prueba5.jpg")
image = imutils.resize(image, width=750)

#cv2.imshow("Imagen Original", image)
#cv2.waitKey(0)
#cv2.destroyAllWindows()
# define the list of boundaries

#BGR

rangos_colores = [
    ([0, 0, 150], [150, 112, 255], "Rojo"),
    ([85, 0, 0], [255, 150, 96], "Azul"),
    ([0, 128, 0], [153, 255, 153], "Verde"),
    ([0, 179, 179], [128, 255, 255], "Amarillo"),
    ([100, 0, 89], [255, 110, 255], "Morado"),
    ([230, 200, 0], [255, 255, 179], "Celeste")
]


# loop over the boundaries
for (lower, upper, color) in rangos_colores:
    # create NumPy arrays from the boundaries
    lower = np.array(lower, dtype = "uint8")
    upper = np.array(upper, dtype = "uint8")

    # find the colors within the specified boundaries and apply
    # the mask
    mask = cv2.inRange(image, lower, upper)

    
    output = cv2.bitwise_and(image, image, mask = mask)
    detectar_figuras(output, color)
    # show the images
    #cv2.imshow("images", np.hstack([image, output]))


#print("Total de nodos = ", len(todosNodos))
contador = len(todosNodos)
indice = 0

#Coloca un ID a cada figura
while indice < contador:
    todosNodos[indice].setID(indice +1)
    indice += 1

indice = 0

#Añade a cada figura sus respectivos nodos anexos
while indice < contador:
    
    nodoActual = todasConexiones[indice]
    del(todosNodos[0])
    
    for nodoaAgregar in todosNodos:
        nodoActual.agregar_conexiones(nodoaAgregar)
    
    todosNodos.append(nodoActual.cabeza)
    indice += 1

#Crea una imagen donde se muestran las figuras con sus respectivos ID's
imagenNodos = image.copy()
for nodo in todosNodos:
    cv2.putText(imagenNodos, str(nodo.ID), (nodo.cX, nodo.cY), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 2)

indice = 0

#Crea una representación del grafo en un diccionario, para poder ser usado posteriormente con el algoritmo de Prim
while indice < contador:
    grafo[indice +1] = todasConexiones[indice].getConexiones()
    indice += 1

#print(grafo)
main()
