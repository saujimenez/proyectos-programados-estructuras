
# import the necessary packages
from shapedetector import ShapeDetector
import argparse
import imutils
import cv2

class Nodo:
    def __init__(self, figura, color = None, siguiente = None):
        self.figura = figura
        self.color = color
        self.siguiente = siguiente
    
    def toString(self):
        print("Figura: ", self.figura)
        print("Color: ", self.color)
        print("-----------------")

class Conexion_Figura:
    
    def __init__(self, figura):
        self.cabeza = figura
        self.siguiente = None
        
    def agregar_conexion(self, nuevaFigura):
        temp = self.cabeza
        
        if nuevaFigura.figura == self.cabeza.figura:
            while temp.siguiente != None:
                temp = temp.siguiente
            temp.siguiente = nuevaFigura
            del temp
        
        elif nuevaFigura.color == self.cabeza.color:
            while temp.siguiente != None:
                temp = temp.siguiente
            temp.siguiente = nuevaFigura
            del temp
        
        else:
            del temp

 







# construct the argument parse and parse the arguments
#ap = argparse.ArgumentParser()
#ap.add_argument("-i", "--/shapes_and_colors.jpg", required=True,
#	help="path to the input image")
#args = vars(ap.parse_args())

# load the image and resize it to a smaller factor so that
# the shapes can be approximated better
image = cv2.imread("jeremy.jpg")
resized = imutils.resize(image, width=300)
ratio = image.shape[0] / float(resized.shape[0])

# convert the resized image to grayscale, blur it slightly,
# and threshold it
gray = cv2.cvtColor(resized, cv2.COLOR_BGR2GRAY)
#blurred = cv2.GaussianBlur(gray, (5, 5), 0)
#thresh = cv2.threshold(blurred, 230, 255, cv2.THRESH_BINARY)[1]
_, threshold = cv2.threshold(gray, 240, 255, cv2.THRESH_BINARY)
cv2.imshow("Threshold", threshold)
cv2.waitKey(0)
""
cnts = cv2.findContours(threshold, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

# find contours in the thresholded image and initialize the
# shape detector
#cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)

sd = ShapeDetector()
print(len(cnts))
nodos = []

# loop over the contours
for c in cnts[1:]:
    # compute the center of the contour, then detect the name of the
    # shape using only the contour
    M = cv2.moments(c)
    cX = int((M["m10"] / M["m00"]) * ratio)
    cY = int((M["m01"] / M["m00"]) * ratio)
    shape = sd.detect(c)
    nodo_actual = Nodo(shape)
    nodos.append(nodo_actual)
    # multiply the contour (x, y)-coordinates by the resize ratio,
    # then draw the contours and the name of the shape on the image
    c = c.astype("float")
    c *= ratio
    c = c.astype("int")
    cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
    cv2.putText(image, shape, (cX, cY), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)

    # show the output image
    cv2.imshow("Image", image)
    cv2.waitKey(0)

for figura in nodos:
    figura.toString()

       
            
            
            
            
            
            
            
            
            
            
            
            
            
