/*
Estudiantes: 
-Kevin Zumbado Cruz 2019258634
-Saul Jimenez González 2019027180
-Randy Conejo Juárez. 2019066448
https://es.wikibooks.org/wiki/Programaci%C3%B3n_en_C/Manejo_de_archivos
Estructuras de Datos
Proyecto #2
*/

//Bibliotecas necesarias
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Proyecto2.h"



typedef struct lista{
	struct nodo *direccion;
	struct lista *siguiente;
}lista;



struct lista *cabeza = NULL;
struct nodo *raiz = NULL;

void insertar (char url[50], char alias[20]){

    struct nodo *nuevo = malloc(sizeof(struct nodo));

    strcpy(nuevo->url, url);
    strcpy(nuevo->alias, alias);
    nuevo->nodo_izq = NULL;
    nuevo->nodo_der = NULL;
    nuevo->padre = NULL;
	
	
    struct lista *nuevaDireccion = malloc(sizeof(struct lista));
    nuevaDireccion->siguiente=NULL;	
    nuevaDireccion->direccion=nuevo;	
    
    if(cabeza==NULL){	
	cabeza= nuevaDireccion;
    }else{
	
	struct lista *tmp;
	tmp=cabeza;

	while (tmp->siguiente!=NULL){
		tmp=tmp->siguiente;
	}
	tmp->siguiente=nuevaDireccion;
    }


    if (raiz == NULL){
        raiz = nuevo;
    }else{
        
        struct nodo *antr, *reco;
        
        antr = NULL;
        reco = raiz;
        
        while (reco != NULL){
            antr = reco;
            if (strcmp(url, reco->url) == 0  || strcmp(url, reco->url) < 0){
                reco = reco->nodo_izq;
            }else{
                reco = reco->nodo_der;
            }
        }
	
	

        if (strcmp(url, antr->url) == 0  || strcmp(url, antr->url) < 0){
            antr->nodo_izq = nuevo;
        }else{
            antr->nodo_der = nuevo;
        }
	
        nuevo->padre = antr;
    }

}


void insertarURL (char url[50]){

    struct nodo *nuevo = malloc(sizeof(struct nodo));

    strcpy(nuevo->url, url);
    strcpy(nuevo->alias, "");
    nuevo->nodo_izq=NULL;
    nuevo->nodo_der=NULL;
    nuevo->padre=NULL;

    if (raiz == NULL){
        raiz = nuevo;
    }else{
    
        struct nodo *antr, *reco;
        
        antr = NULL;
        reco = raiz;
        
        while (reco!=NULL){
            antr=reco;
            if (strcmp(url, reco->url)==0  || strcmp(url, reco->url)<0){
                reco=reco->nodo_izq;
            }else{
                reco=reco->nodo_der;		
            }
        }
        
        if (strcmp(url, antr->url)==0  || strcmp(url, antr->url)<0){
            nuevo->padre=antr;
            antr->nodo_izq=nuevo;
        }else{
            nuevo->padre=antr;	
            antr->nodo_der=nuevo;	
        }
    }

}



//Función para eliminar un nodo del árbol  
void eliminar(struct nodo *top, char URL[50]){    
  if (top == NULL){ //Si no hay nada en el arbol no se hace nada
    return;

  }else if(strcmp(URL,top->url)<0){ //Si el valor es menor busca por la izquierda
    eliminar(top->nodo_izq, URL);

  }else if(strcmp(URL,top->url)>0){ // Si el valor es mayor, busca por la derecha
    
    eliminar(top->nodo_der, URL);

  }else {
    printf("\nRaiz= %s\n",top->url);
    eliminarNodo(top);
  }
   
}


void eliminarNodo (struct nodo *nodoEliminar){
  
if(nodoEliminar->nodo_izq && nodoEliminar->nodo_der){ //Si tiene hijo der e hijo izq
    
    struct nodo *menor = minimo(nodoEliminar->nodo_der);
    
    strcpy(nodoEliminar->url,menor->url);
    eliminarNodo(menor);
  
  }else if(nodoEliminar->nodo_izq){ //Si tiene hijo izq
    

    reemplazar(nodoEliminar,nodoEliminar->nodo_izq);
    destruirNodo(nodoEliminar);

  
  }else if(nodoEliminar->nodo_der){ //Si tiene hijo der
    
	if (raiz!=nodoEliminar){
	    reemplazar(nodoEliminar,nodoEliminar->nodo_der);
	    destruirNodo(nodoEliminar);
	}else{
	    struct nodo *menor = minimo(nodoEliminar->nodo_der);
    
	    strcpy(nodoEliminar->url,menor->url);
	    eliminarNodo(menor);
	}
  }else{ //Si no tiene hijos
    reemplazar(nodoEliminar,NULL);
    
    if(raiz!=nodoEliminar){
    	destruirNodo(nodoEliminar);
    }else{
	raiz=NULL;
    }
  }

}

//Funcion para determinar el nodo mas a la izquierda
struct nodo *minimo (struct nodo *top){
  if (top == NULL){
    return NULL;
  }
  if (top->nodo_izq){
    return minimo(top->nodo_izq);
    
  } else{
      return top;
  }
}

//Funcion para reemplazar un nodo 
void reemplazar(struct nodo *top, struct nodo *nuevoNodo){

  if(top->padre){
    if(top==top->padre->nodo_izq){

      top->padre->nodo_izq = nuevoNodo;

    }else if(top==top->padre->nodo_der){

      top->padre->nodo_der = nuevoNodo;

    }
  }
	
  if(nuevoNodo==NULL){
	top=NULL;
  }

  if(nuevoNodo!=NULL){
    nuevoNodo->padre = top->padre;
	if(nuevoNodo->padre==NULL){
		raiz=nuevoNodo;	
	}
  }
}

//Funcion para destruir un nodo
void destruirNodo(struct nodo *nodoArbol){
  nodoArbol = NULL;
  free(nodoArbol);
}


void imprimirInOrden(struct nodo *reco){
    if (reco!=NULL){
        imprimirInOrden(reco->nodo_izq);
        printf("%s->",reco->url);
        imprimirInOrden(reco->nodo_der);
    }
}

void buscarAlias(char alias[20]){
    
    lista *temp = NULL;
    temp = malloc(sizeof(lista));
    temp = cabeza;
    char comando[50] = "xdg-open ";
    
    while (temp != NULL){
        
        if ((strcmp(temp->direccion->alias, alias)) == 0){
            printf("Abriendo en su navegador predeterminado %s\n", alias);
            strcat(comando,temp->direccion->url);
            system(comando);
            return;
        }
        else {
            temp = temp->siguiente;
        }
        
    }
    
    printf("\nEl alias ingresado no fue encontrado\n");
    
}

int buscarURL(char URL[50]){
    
    nodo *temp = NULL;
    temp = malloc(sizeof(nodo));
    temp = raiz;
    
    while (temp != NULL){
        
        if ((strcmp(temp->url, URL)) == 0){
            return 1;
        }
        else if ((strcmp(temp->url, URL)) > 0){
            temp = temp->nodo_izq;
        }
        else {
            temp = temp->nodo_der;
        }
        
    }
    
    return 0;
    
}


void ingresarURLS(){
    
    FILE *archivo;
    char linea[100];
    char *tok;
    int contador = 0;
    char URL[50] = "https://";
    system("axel https://moz.com/top-500/download/?table=top500Domains");

    archivo = fopen("top500Domains.csv","r");
    
    fgets(linea, 100, archivo);
    
    while (contador < 500){
        
        fgets(linea, 100, archivo);
        tok = strtok(linea, ",");
        tok = strtok(NULL, ",");
        tok = strtok(tok, "\"");
        strcat(URL, tok);
        insertarURL(URL);
        printf("Insertado: %s\n", URL);
        contador ++;
        strcpy(URL, "https://");
        
    }

    fclose(archivo);
}

void imprimirLista(){
	struct lista *tmp;
	tmp=cabeza;
	while (tmp!=NULL){
		printf("Alias: %s \n",tmp->direccion->alias);
		tmp=tmp->siguiente;	
	}
}

void cambiar_alias(char URL[50]){
    nodo *temp = NULL;
    temp = malloc(sizeof(nodo));
    temp = raiz;
    char alias[25];
    if (buscarURL(URL) == 1){
        
        while (temp != NULL){
        
            if ((strcmp(temp->url, URL)) == 0){
                printf("\nIngrese el nuevo alias de %s:", temp->url);
                scanf("%s", alias);
                if (strcmp(temp->alias, "") == 0){
                    strcpy(temp->alias, alias);
                    struct lista *nuevaDireccion = malloc(sizeof(struct lista));
                    nuevaDireccion->siguiente = NULL;
                    nuevaDireccion->direccion = temp;
    
                    if(cabeza == NULL){	
                        cabeza = nuevaDireccion;
                    }else{
        
                    struct lista *tmp;
                    tmp = cabeza;
    
                    while (tmp->siguiente != NULL){
                        tmp = tmp->siguiente;
                    }
                    tmp->siguiente = nuevaDireccion;
                    }
                }else{
                    strcpy(temp->alias, alias);
                }
                break;
            }
            else if ((strcmp(temp->url, URL)) > 0){
                temp = temp->nodo_izq;
            }
            else {
                temp = temp->nodo_der;
            }   
        
        }
    }
    else{
        printf("\nEl URL ingresado no fue encontrado");
    }
        
}


int main() {
    /*
    Funcion principal que actua como la funcion nexo de las demas
    controla el flujo del programa
    */
    int loop=0;
    int opcion;
    char comando[50] = "xdg-open ";
    

  
  char url[50];
  char alias[20];
    //Menu
    while(loop==0){
    

        printf("\nBienvenido al sistema de Paginas WEB");
        printf("\n1.\tIngresar Direccion con alias");
        printf("\n2.\tIngresar Direccion");
        printf("\n3.\tAgregar 500 direcciones");
        printf("\n4.\tImprimir Arbol");
        printf("\n5.\tBuscar por URL");
        printf("\n6.\tBuscar por alias");
        printf("\n7.\tEliminar dirección");
        printf("\n8.\tAgregar/Editar alias");
        printf("\n0.\tSalir\n");
        printf("Ingrese su seleción: ");
        scanf("%d",&opcion);
        

        switch(opcion){
            case 1:
                printf("\nIngrese una direccion nueva: ");
                scanf("%s",url);
                printf("\nIngrese el alias de la dirección: ");
                scanf("%s", alias);
                insertar(url, alias);
                break;
            case 2:
                printf("\nIngrese una direccion nueva: ");
                scanf("%s",url);
                insertarURL(url);
                break;

            case 3:
                ingresarURLS();
                printf("Se añadieron las 500 URL's más visitadas\n");
                break;

            case 4:
                printf("\n");
                imprimirInOrden(raiz);
                printf("NULL\n");
                printf("Raiz actual: %s",raiz->url);
                break;

            case 5: 
                printf("\nIngrese el URL a que desea abrir: ");
                scanf("%s", url);
                if (buscarURL(url) == 1){
                    printf("Abriendo en su navegador predeterminado %s\n", url);
                    strcat(comando, url);
                    system(comando);
                    strcpy(comando, "xdg-open ");
                }
                else{
                    printf("\nEl URL ingresado no fue encontrado");
                }
                break;
            
            case 6:
                printf("\nLista de páginas\n");
                imprimirLista();
                printf("\nIngrese el Alias que desea abrir: ");
                scanf("%s", alias);
                buscarAlias(alias);
                break;

            case 7: 
                printf("\nIngrese el URL que desea eliminar: ");
                scanf("%s", url);
                eliminar(raiz,url);
                break;
            
            case 8:
                printf("\nIngrese el URL del la página: ");
                scanf("%s", url);
                cambiar_alias(url);
                break;

            case 0:
                loop = 1;
                break;

            
        }
    }

    return 0;
 
}
