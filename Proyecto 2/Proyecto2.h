
typedef struct nodo{

    char url[50];
    char alias[20];
    struct nodo *nodo_izq;
    struct nodo *nodo_der;
    struct nodo *padre;

}nodo;

typedef struct nodoAlias{
    
    struct nodo *nodo_alias;
    struct nodoAlias *sig;
    
}nodoAlias;


//Declaracion de Funciones

/*----------------------------------------------------------------------
	destruirNodo
	Entradas: Un puntero tipo nodo con el nodo que se desea eliminar del programa
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para liberar el espacio que ocupa el nodo que se elimino del arbol
					Hace que el nodo indicado no tenga hijos y lo libera (free)
 
 ---------------------------------------------------------------------*/
void destruirNodo(struct nodo *nodoArbol);

/*----------------------------------------------------------------------
	eliminarNodo
	Entradas: Un puntero tipo nodo con el nodo que se va a eliminar
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para eliminar el nodo indicado del arbol
					Valida cualquier posibilidad de que el nodo tenga hijos
					Busca el nodo por el cual reemplazar el nodo a eliminar
					Elimina del arbol el nodo.
 
 ---------------------------------------------------------------------*/
void eliminarNodo(struct nodo *nodoArbol);

/*----------------------------------------------------------------------
	minimo
	Entradas: Un puntero tipo nodo a la raiz del arbol
	Salidas: Un nodo del arbol indicando el menor el elemento por el cual reemplazar el nodo a eliminar
	Funcionamiento: Sirve para indicarle a reemplazar, el nodo por el cual hay que reemplazar
					Si el arbol esta vacio, se retorna NULL
					Si la raiz tiene hijo izquierdo, se retorna la funcion, ahora con este hijo izquierdo
					Este proceso se repite hasta que el parametro de entrada no tenga hijo izquierdo.
					Se retorna el ultimo nodo que se uso de entrada.
 
 ---------------------------------------------------------------------*/
struct nodo *minimo(struct nodo *top);

/*----------------------------------------------------------------------
	reemplazar
	Entradas: Dos punteros tipo nodo apuntando al nodo a eliminar y al nodo por el que va a reemplazar
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para reemplazar el nodo a eliminar por el nodo indicado
					Si el nodo tiene padre, se valida que
					Si el url del nodo es igual al izquierdo del padre, este nodo sera el nuevo
					Si el url del nodo es igual al del hijo derecho del padre, este nodo sera el nuevo
					Si existe un nuevo nodo, el padre de este, sera el padre del nodo a eliminar.
 
 ---------------------------------------------------------------------*/
void reemplazar(struct nodo *top, struct nodo *nuevoNodo);

/*----------------------------------------------------------------------
	ingresarURLS
	Entradas: Sin parametros de entrada
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para ingresar al arbol los 500 links de la pagina
					Ingresa cada link y les pone https:// antes, tambien le quita las comillas a los links.
 
 ---------------------------------------------------------------------*/
void ingresarURLS();

/*----------------------------------------------------------------------
	insertarURL
	Entradas: Un url tipo char limitado a 50 espacios
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para insertar un nuevo nodo solo con un  URL.
					Crea un nuevo nodo y lo inserta
					Si el arbol esta vacio, lo inserta en la raiz
					De otra forma, valida si es menor o mayor que cada nodo por el que se tope hasta llegar a una posicion en NULL
 
 ---------------------------------------------------------------------*/
void insertarURL (char url[50]);
/*----------------------------------------------------------------------
	insertar
	Entradas: Un url tipo char limitado a 50 espacios y el alias limitado a 20
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para insertar un nuevo nodo con un  URL y un alias
					Crea un nuevo nodo y lo inserta
					Si el arbol esta vacio, lo inserta en la raiz
					De otra forma, valida si es menor o mayor que cada nodo por el que se tope hasta llegar a una posicion en NULL
 
 ---------------------------------------------------------------------*/
void insertar (char url[50], char alias[20]);

/*----------------------------------------------------------------------
	buscarURL
	Entradas: Un URL tipo char limitado a 50 espacios
	Salidas: Sin valores de retorno
	Funcionamiento: Sirve para buscar en el arbol una URL que ya ha sido guardada para abrirla
					Crea un nodo temporal con el url ingresado y lo compara con cada elemento del arbol hasta encontrar el que sea igual
					Abre el link en el navegador predeterminado del usuario
					Si no lo encuentra imprime un mensaje diciendo esto.

 ---------------------------------------------------------------------*/
int buscarURL(char URL[50]);

/*----------------------------------------------------------------------
	buscarAlias
	Entradas: Un alias tipo char limitado a 25 espacios
	Salidas: retorna un número entero indicando si se encontró o no el url
	Funcionamiento: Sirve para buscar un elemento del arbol por su alias y abrirlo
 
 ---------------------------------------------------------------------*/
void buscarAlias(char alias[25]);

/*----------------------------------------------------------------------
	imprimirInOrden
	Entradas: la raiz del arbol
	Salidas: no retorna nada
	Funcionamiento: Sirve para imprimir el arbol en orden
 
 ---------------------------------------------------------------------*/
void imprimirInOrden(struct nodo *reco);
/*----------------------------------------------------------------------
	imprimirLista
	Entradas: no recibe nada
	Salidas: no retorna nada
	Funcionamiento: Sirve para imprimir una lista con los urls que tienen alias
 
 ---------------------------------------------------------------------*/
void imprimirLista();
/*----------------------------------------------------------------------
	cambiar_alias
	Entradas: recibe un url
	Salidas: no retorna nada
	Funcionamiento: Busca el url indicado por el usuario y cambia su alias si lo encuentra.
 
 ---------------------------------------------------------------------*/
void cambiar_alias(char URL[50]);
